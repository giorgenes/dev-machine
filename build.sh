#!/bin/bash

packer build \
    -var "aws_access_key=$AWS_ACCESS_KEY" \
    -var "aws_secret_key=$AWS_ACCESS_TOKEN" \
    -var "version=0.1" \
    packer.json

